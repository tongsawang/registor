package com.example.registor.junit;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;

@Execution(ExecutionMode.CONCURRENT)
//@Execution(ExecutionMode.SAME_THREAD) //*Default
@ExtendWith(StopwatchExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public interface CommonTests {

    @BeforeAll
    default void beforeAllTests() {
        System.out.println(String.format("[%s] Before all tests", Thread.currentThread().getName()));
    }

    @AfterAll
    default void afterAllTests() {
        System.out.println(String.format("[%s] After all tests", Thread.currentThread().getName()));
    }

    @BeforeEach
    default void beforeEachTest(TestInfo info) {
        System.out.println(String.format("[%s] Before execute [%s]", Thread.currentThread().getName(),
                info.getDisplayName()));
    }

    @AfterEach
    default void afterEachTest(TestInfo info) {
        System.out.println(String.format("[%s] Finished executing [%s]", Thread.currentThread().getName(),
                info.getDisplayName()));
    }
}

