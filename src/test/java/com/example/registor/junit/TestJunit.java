package com.example.registor.junit;

import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.condition.EnabledOnJre;
import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.JRE;
import org.junit.jupiter.api.condition.OS;

@DisplayName("Tests Demo Using JUnit")
class TestJunit implements CommonTests{
	  @Nested
	    @DisplayName("Basic Tests Demo")
	    class Basic {
	        @Test
	        @DisplayName("Should_Success_When_ProductIsValid")
	        void test_method1(TestInfo info) throws InterruptedException {
	            TimeUnit.MILLISECONDS.sleep(500);
	            System.out.println(String.format("[%s] Execute [%s]", Thread.currentThread().getName(), info.getDisplayName()));
	        }

	        @Test
//	        @Disabled
	        @DisplayName("Should_ThrowException_When_ProductIsInvalid")
	        void test_method2(TestInfo info) throws InterruptedException {
	            TimeUnit.MILLISECONDS.sleep(500);
	            System.out.println(String.format("[%s] Execute [%s]", Thread.currentThread().getName(), info.getDisplayName()));
	        }

	        @Test
	        @EnabledOnOs(OS.WINDOWS)
	        @EnabledOnJre(JRE.JAVA_8)
	        void test_method3(TestInfo info) throws InterruptedException {
	            TimeUnit.MILLISECONDS.sleep(500);
	            System.out.println(String.format("[%s] Execute [%s]", Thread.currentThread().getName(), info.getDisplayName()));
	        }

	        //
	        @Test
	        @EnabledOnOs(OS.MAC)
	        @EnabledOnJre(JRE.JAVA_11)
	        void test_method4(TestInfo info) throws InterruptedException {
	            TimeUnit.MILLISECONDS.sleep(500);
	            System.out.println(String.format("[%s] Execute [%s]", Thread.currentThread().getName(), info.getDisplayName()));
	        }
	    }
}
