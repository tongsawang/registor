package com.example.registor.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.example.registor.Utils.Util;
import com.example.registor.entity.RegistorEntity;
import com.example.registor.junit.StopwatchExtension;
import com.example.registor.service.RegistorService;

import info.solidsoft.mockito.java8.api.WithBDDMockito;


@DisplayName("Test registor controller")
@Execution(ExecutionMode.SAME_THREAD)
@WebMvcTest(RegistorController.class)
@ExtendWith({SpringExtension.class, StopwatchExtension.class, MockitoExtension.class})
public class RegistorControllerTest  implements WithBDDMockito{
	
	@MockBean
	private RegistorService service;
	
	@Autowired
	private MockMvc mockMvc;
	
	private RegistorEntity gen(long id, String username, String fullname, int age) {
		return new RegistorEntity(id, username, fullname, age);
	}
	
	@Test
	@WithMockUser(username = "john", password = "password")
    void should_SuccessToFindAll() throws Exception {
        // given
        List<RegistorEntity> registors = Arrays.asList(
        	gen(1L,"UsernameAAA", "John Wick", 35),
        	gen(2L,"UsernameBBB", "The Rock", 45)
        );
        given(service.findAll()).willReturn(registors);

        // when
        mockMvc.perform(get("/api/v1/registors/")
                .headers(getHttpHeaders()))
                .andDo(print())
                 // then
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.data", Matchers.hasSize(2)))
                .andExpect(jsonPath("$.data[0].username", Matchers.is("UsernameAAA")))
                .andExpect(jsonPath("$.data[0].fullname", Matchers.is("John Wick")))
                .andExpect(jsonPath("$.data[0].age", Matchers.is(35)))
                .andExpect(jsonPath("$.data[1].username", Matchers.is("UsernameBBB")))
                .andExpect(jsonPath("$.data[1].fullname", Matchers.is("The Rock")))
        		.andExpect(jsonPath("$.data[1].age", Matchers.is(45)));
   }
	
   @Test
   @WithMockUser(username = "john", password = "password")
   void should_SuccessToSave() throws Exception {

       // given
	   RegistorEntity registor = new RegistorEntity(1L, "UsernameAAA", "John wick", 35);
	   given(service.save(any(RegistorEntity.class))).willReturn(registor);
	   
	   // when
	   mockMvc.perform(post("/api/v1/registors/")
               .headers(getHttpHeaders())
               .contentType(MediaType.APPLICATION_JSON)
               .content(Util.asJsonString(registor)))
               .andDo(print())
               // then
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("$.code", Matchers.is("xxx-200")))
               .andExpect(jsonPath("$.message", Matchers.is("created")))
               .andExpect(jsonPath("$.data.username", Matchers.is("UsernameAAA")))
               .andExpect(jsonPath("$.data.fullname", Matchers.is("John wick")))
               .andExpect(jsonPath("$.data.age", Matchers.is(35)));
   }

   private HttpHeaders getHttpHeaders() {
       HttpHeaders headers = new HttpHeaders();
       headers.add("x-Access-Token", "27bb0a9c");
       return headers;
   }
}
