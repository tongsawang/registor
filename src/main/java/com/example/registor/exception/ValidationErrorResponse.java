package com.example.registor.exception;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ValidationErrorResponse {
	private List<Violation> violations = new ArrayList<>();
}
