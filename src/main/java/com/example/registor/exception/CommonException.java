package com.example.registor.exception;

import org.springframework.http.HttpStatus;

import lombok.Getter;

@Getter
public abstract class CommonException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	protected final HttpStatus status;
    protected final String code;

    CommonException(HttpStatus status, String code, String message) {
        super(message);
        this.status = status;
        this.code = code;
    }
}

