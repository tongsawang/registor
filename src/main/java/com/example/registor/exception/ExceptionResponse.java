package com.example.registor.exception;

import java.time.OffsetDateTime;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExceptionResponse {
    private String code;
    private String message;
    private OffsetDateTime timestamp = OffsetDateTime.now();
}
