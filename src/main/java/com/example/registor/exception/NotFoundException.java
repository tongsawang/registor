package com.example.registor.exception;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class NotFoundException extends CommonException {
	
    public NotFoundException(String message) {
        super(HttpStatus.NOT_FOUND, "404", message);
    }
}

