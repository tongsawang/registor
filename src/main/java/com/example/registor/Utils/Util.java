package com.example.registor.Utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Util {
	public static String setMethodName(String className,String methodName){
        String [] names = className.split("\\$")[0].split("\\.");
        return "["+names[names.length-1]+"."+methodName+"]=> ";
    }
	
	public static String asJsonString(final Object obj) throws JsonProcessingException {
	       return new ObjectMapper().writeValueAsString(obj);
	}
}
