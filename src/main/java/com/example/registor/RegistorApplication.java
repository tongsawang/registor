package com.example.registor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RegistorApplication {

	public static void main(String[] args) {
		SpringApplication.run(RegistorApplication.class, args);
	}

}
