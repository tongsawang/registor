package com.example.registor.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.registor.entity.RegistorEntity;

@Repository
public interface RegistorRepository extends CrudRepository<RegistorEntity, Long>{
	List<RegistorEntity> findAll();
	
	@Modifying(clearAutomatically=true)
    @Query(value = "UPDATE Registor SET age = :age WHERE id = :id", nativeQuery = true)
    int updateAge(@Param("id") long id, @Param("age") int age);
}
