package com.example.registor.service;

import javax.annotation.PostConstruct;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class CommonService {
	@PostConstruct
    protected void postConstruct() {
        log.info(getClass().getSimpleName() + " : Initialized()");
    }
}
