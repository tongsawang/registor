package com.example.registor.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.registor.entity.RegistorEntity;
import com.example.registor.exception.NotFoundException;
import com.example.registor.repository.RegistorRepository;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class RegistorService extends CommonService{
	@Autowired
	RegistorRepository registorRepository;
	
	//Mock up data
	public Iterable<RegistorEntity> create() {
		log.info(() -> "Mock up Create all");
		
		//Mock up
		List<RegistorEntity> entities = new ArrayList<>();
		RegistorEntity entity1 = new RegistorEntity();
		entity1.setUsername("UserA");
		entity1.setFullname("AAA");
		entity1.setAge(28);
		
		RegistorEntity entity2 = new RegistorEntity();
		entity2.setUsername("UserB");
		entity2.setFullname("BBB");
		entity2.setAge(19);
		
		entities.add(entity1);
		entities.add(entity2);
		
		return registorRepository.saveAll(entities);
	}
	
	//Save
	public RegistorEntity save(RegistorEntity entity) {
		log.info(() -> "Save");
		return registorRepository.save(entity);
	}
	
	//Read
	public List<RegistorEntity> findAll(){
		log.info(() -> "Read");
		return registorRepository.findAll();
	}
	//Find by id
	public RegistorEntity find(long id) {
		log.info(() -> "Find id "+id);
		Optional<RegistorEntity> optional = registorRepository.findById(id);
		if(optional.isPresent()) {
			return optional.get();
		}else {
			throw new NotFoundException("Registor not found."); 
		}
	}
	//Update
	@Transactional
	public int updateAge(long id, int age) {
		log.info(() -> "Update age id "+id + ", age "+age);
		return registorRepository.updateAge(id, age);
	}
	
	//Delete
	public void delete(Long id) {
        log.info(() -> "Delete id " + id);
        registorRepository.deleteById(id);
    }
}
