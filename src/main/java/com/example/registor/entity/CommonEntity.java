package com.example.registor.entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

@Getter(AccessLevel.PROTECTED)
@Setter(AccessLevel.PROTECTED)
@MappedSuperclass
@EntityListeners(value = CommonListeners.class)
public abstract class CommonEntity {
	@Column(name="createdDate")
    private LocalDateTime createdDate;
	@Column(name="updatedDate")
    private LocalDateTime updatedDate;

}
