package com.example.registor.entity;

import java.time.LocalDateTime;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

public class CommonListeners<T extends CommonEntity> {
    @PrePersist
    private void prePersist(T e) {
        e.setCreatedDate(LocalDateTime.now());
    }

    @PreUpdate
    private void preUpdate(T e) {
        e.setUpdatedDate(LocalDateTime.now());
    }

}

