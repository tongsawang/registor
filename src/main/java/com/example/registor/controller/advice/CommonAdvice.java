package com.example.registor.controller.advice;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.example.registor.Utils.Util;
import com.example.registor.exception.CommonException;
import com.example.registor.exception.ExceptionResponse;

import io.undertow.util.BadRequestException;
import lombok.extern.log4j.Log4j2;
import java.util.HashSet;

@Log4j2
@RestControllerAdvice
public class CommonAdvice extends ResponseEntityExceptionHandler {

    private ResponseEntity<Object> handle(Exception ex, HttpStatus status, String code) {
        ex.printStackTrace();
        ExceptionResponse response = new ExceptionResponse();
        response.setCode("xxx-" + code);
        response.setMessage(status.getReasonPhrase());
        HttpHeaders headers = new HttpHeaders();
        headers.add("x-Information", "https://domain-name/context-path/api/v1/errors/" + code);
        return ResponseEntity.status(status).headers(headers).body(response);
    }
    
    private ResponseEntity<Object> handleMsg(Exception ex, HttpStatus status, String code, String msg) {
        ex.printStackTrace();
        ExceptionResponse response = new ExceptionResponse();
        response.setCode("xxx-" + code);
        response.setMessage(msg);
        HttpHeaders headers = new HttpHeaders();
        headers.add("x-Information", "https://domain-name/context-path/api/v1/errors/" + code);
        return ResponseEntity.status(status).headers(headers).body(response);
    }
    
    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return handle(ex, status, String.valueOf(status.value()));
    }

    @ExceptionHandler(CommonException.class)
    protected ResponseEntity<Object> handle(CommonException ex) {
        return handle(ex, ex.getStatus(), ex.getCode());
    }

    @ExceptionHandler(Exception.class)
    protected ResponseEntity<Object> handle(Exception ex) {
        return handle(ex, HttpStatus.INTERNAL_SERVER_ERROR, String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
    }
    
    @ExceptionHandler({IllegalStateException.class, IllegalArgumentException.class})
    protected ResponseEntity<Object> handleIllegal(RuntimeException ex) {
        return handle(ex, HttpStatus.BAD_REQUEST, String.valueOf(HttpStatus.BAD_REQUEST.value()));
    }
    
    @ExceptionHandler(ConstraintViolationException.class)
    protected ResponseEntity<Object> handleConstraintViolationException(
      HttpServletRequest request, ConstraintViolationException ex
    ) {
    	return handleMsg(ex, HttpStatus.BAD_REQUEST, String.valueOf(HttpStatus.BAD_REQUEST.value()), ex.getMessage());
    }
   
    
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {

        String methodName =Util.setMethodName(ex.getStackTrace()[0].getClassName(),ex.getStackTrace()[0].getMethodName());
        log.error(methodName  +"ex.getLineNumber: "+ex.getStackTrace()[0].getLineNumber());
        log.error(methodName,ex);

        List<String> errors = new ArrayList<String>();
        for (FieldError error : ex.getBindingResult().getFieldErrors()) {
            errors.add(error.getField() + ": " + error.getDefaultMessage());
        }
        for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
            errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
        }
        ExceptionResponse response = new ExceptionResponse();
        response.setCode(HttpStatus.BAD_REQUEST.value()+"");
        response.setMessage(errors.toString());

        return  ResponseEntity.status(status).headers(headers).body(response);

    }


}