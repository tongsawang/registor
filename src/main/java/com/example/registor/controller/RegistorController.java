package com.example.registor.controller;

import static org.springframework.http.ResponseEntity.ok;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.registor.entity.RegistorEntity;
import com.example.registor.service.RegistorService;

import static com.example.registor.dto.response.SuccessResponse.builder;

@Validated
@RestController
public class RegistorController extends CommonController{
	@Autowired
	RegistorService service;
	
	@GetMapping("/registors/mock")
	public ResponseEntity<?> mockup() {
		return ok(builder(service.create()).build());
	}
	
	@GetMapping("/registors/")
	public ResponseEntity<?> getAll() {
		return ok(builder(service.findAll()).build());
	}
	
	@GetMapping("/registors/{id}")
	public ResponseEntity<?> get(@PathVariable Long id){
		return ok(builder(service.find(id)).build());
	}
	
	@PostMapping("/registors/")
    public ResponseEntity<?> save(@Valid @RequestBody RegistorEntity entity) {
        RegistorEntity response = service.save(entity);
        return ok(builder(service.save(response)).message("created").build());
    }
	
	@PatchMapping("/registors/{id}/age/{age}")
	public ResponseEntity<?> updateAge(@PathVariable @Min(18) @Min(150) Long id, @PathVariable int age){
		return ok(builder(service.updateAge(id, age)).build());
	}
	
	@DeleteMapping("/registors/{id}")
	public void delete(@PathVariable Long id) {
		service.delete(id);
	}
}
