package com.example.registor.controller;

import javax.annotation.PostConstruct;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import lombok.extern.log4j.Log4j2;

@Log4j2
@CrossOrigin
@RequestMapping("/api/v1")
public class CommonController {
	@PostConstruct
    protected void postConstruct() {
        log.info(getClass().getSimpleName() + " : Initialized()");
    }
}
